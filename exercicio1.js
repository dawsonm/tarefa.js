// a. Caso teste 1 : [ [ [2],[-1] ], [ [2],[0] ] ] e [ [2,3],[-2,1] ] multiplicadas dão
// [ [6,5], [4,6] ]
// b. Caso teste 2 : [ [4,0], [-1,-1] ] e [ [-1,3], [2,7] ] multiplicadas dão [
// [-4,12], [-1,-10] ]

let m1 = [[2,-1,3], [2,0,4]]
let m2 = [[2,3],[-2,1]]

let m4 = [[4,0],[-1,-1]]
let m5 = [[-1,3], [2,7]]

function multMatrizes(m4,m5){
        return m4[0].length == m5.length
    
}

if (multMatrizes(m1,m5)){


    var mnew = []
    let soma = 0
    let NLA = m4.length
    let NCB = m5[0].length
    let NLB = m5.length

    for(let i = 0; i<NLA; i++){
        mnew.push([])
        for(let j = 0; j<NCB; j++){
            
            for(let k = 0; k<NLB;k++){
                soma += m4[i][k]*m5[k][j]
            }
            mnew[i].push(soma)
            soma = 0;
        }           
    }
}else{
    console.log("Dá errado.")
}
console.log(mnew)